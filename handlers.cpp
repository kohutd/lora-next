#include "handlers.h"
#include "pages.h"

void handleRoot(WebServer *server) {
  (*server).send(200, "text/html", rootPage());
}

void handleSetPin(WebServer *server) {
  int pathPin = (*server).pathArg(0).toInt();
  String pathMode = (*server).pathArg(1);
  pathMode.toUpperCase();

  switch (pathMode[0]) {
    case 'I': pinMode(pathPin, INPUT);
      break;
    case '0': pinMode(pathPin, INPUT);
      break;

    case 'O': pinMode(pathPin, OUTPUT);
      break;

    case '1': pinMode(pathPin, OUTPUT);
      break;
  }

  Serial.println("Set pin ");
  Serial.print(pathPin);
  Serial.print(" ");
  Serial.print(pathMode);

  (*server).send(200, "text/html", pathMode);
}

void handleDigWrite(WebServer *server) {
  int pathPin = (*server).pathArg(0).toInt();
  int pathMode = (*server).pathArg(1).toInt();

  Serial.println("Digital write ");
  Serial.print(pathPin);
  Serial.print(" ");
  Serial.print(pathMode);

  digitalWrite(pathPin, pathMode);
  (*server).send(200, "text/html", (String) pathMode);
}

void handleDigRead(WebServer *server) {
  int pathPin = (*server).pathArg(0).toInt();

  (*server).send(200, "text/html", (String) digitalRead(pathPin));
}

void handleAnalRead(WebServer *server) {
  int pathPin = (*server).pathArg(0).toInt();

  (*server).send(200, "text/html", (String) analogRead(pathPin));
}
