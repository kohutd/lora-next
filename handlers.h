#include <WebServer.h>

void handleRoot(WebServer *server);
    
void handleSetPin(WebServer *server);
void handleDigWrite(WebServer *server);
void handleDigRead(WebServer *server);
void handleAnalRead(WebServer *server);
