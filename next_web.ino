#include <WiFi.h>
#include <WiFiClient.h>
#include <WebServer.h>
#include <ESPmDNS.h>

#include <U8x8lib.h>

#include "handlers.h"


#define LED 25

#define SERIAL_PORT 115200

#define NEXT_SSID "LORA_NEXT"
#define NEXT_PASSWORD "LORA_NEXT"

//#define NEXT_PIN "1121"

U8X8_SSD1306_128X64_NONAME_SW_I2C u8x8(15, 4, 16); // oled

WebServer server(80);



bool isLed = 0;


void wifiConnect() {
  bool kek = 0;
  
  u8x8.begin();

  u8x8.setFont(u8x8_font_amstrad_cpc_extended_f);    
  u8x8.clear();
  
  u8x8.setFont(u8x8_font_7x14_1x2_r);
  u8x8.drawString(1, 3, "CONNECTING..");
  
  WiFi.mode(WIFI_STA);
  WiFi.begin(NEXT_SSID, NEXT_PASSWORD);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    u8x8.clear();
    u8x8.drawString(1, 3, kek ? "CONNECTING.." : "CONNECTING.");
    kek = !kek;
    Serial.print(".");
  }

  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(NEXT_SSID);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
  
  u8x8.clear();
  
  char buf[64];
  WiFi.localIP().toString().toCharArray(buf, 64);
  u8x8.drawString(1, 3, buf);
}

void handleLed() {
  isLed = !isLed;
  digitalWrite(LED, isLed);
  server.send(200, "text/html", isLed ? "On" : "Off");
}

void setup() {
  pinMode(LED, OUTPUT);
  digitalWrite(LED, 0);
  Serial.begin(SERIAL_PORT);

  wifiConnect();

  server.on("/", []() { handleRoot(&server); });
  server.on("/led", handleLed);
  
  server.on("/pin/{}/set/{}", []() { handleSetPin(&server); });

  server.on("/dig/{}/write/{}", []() { handleDigWrite(&server); });
  server.on("/dig/{}", []() { handleDigRead(&server); });
  
  server.on("/anal/{}", []() { handleAnalRead(& server); });

  server.begin();
}

void loop() {
  server.handleClient();
}
