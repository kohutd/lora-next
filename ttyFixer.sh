# script that fixes bug(?) with rights to usb device
# why? idk. it just works. (c) exp


#!/bin/bash

COLOR_OFF='\e[0m'
WHITE_BOLD='\e[1;37m'

ALREADY_ESTALBISHED=false


echo -en "$(date) $WHITE_BOLD~ waiting..$COLOR_OFF\n"
while true; do
	sleep 1

	if [ $(ls /dev/ | grep USB | head -1) ]; then
		if [ "$ALREADY_ESTALBISHED" = false ]; then
			tty=$(ls /dev/ | grep USB | head -1)
			chmod 666 "/dev/$tty"
			echo -en "$(date) $WHITE_BOLD> connected = $tty$COLOR_OFF\n"
			ALREADY_ESTALBISHED=true
		fi
	else
		if [ "$ALREADY_ESTALBISHED" = true ]; then
			echo -en "$(date) $WHITE_BOLD~ waiting..$COLOR_OFF\n"
			ALREADY_ESTALBISHED=false
		fi
	fi
done
